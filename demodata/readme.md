# Radial Diffusion Spectrum Imaging (RDSI): Demo DWI data

Data simulated with [Phantomas](http://www.emmanuelcaruyer.com/phantomas.php) to use as an example for RDSI reconstruction.

## Obtaining the simulation data

Simulation data can be easily generated with the simulated DWI MRI data and the script rdsi_phantomas_example.m in the [rdsi_recon](https://sbaete@bitbucket.org/sbaete/rdsi_recon.git) repository at [https://sbaete@bitbucket.org/sbaete/rdsi_recon.git](https://sbaete@bitbucket.org/sbaete/rdsi_recon.git) or  cai2r.net -> Resources -> Software Downloads

When done, copy *rdsi_recon/demodata/dwis.src.gz.fib.gz* and *rdsirecon/demodata/dwis.src.gz* to this folder *demodata/*.